<?php
session_start();

// Create a new CSRF token.
if (! isset($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
}
//include the paginator class
include('paginator.php');
?>
<!doctype html>
<html lang=en>
<head>
    <title>Home</title>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php"); ?>
    <div class="primary">
        <div class="post-wrap clearfix">
            <?php
            //The link to the database is moved to the top of the PHP code.
            require('mysqli_connect.php'); // Connect to the db.

            // Make the query:
            $total_query = "SELECT name, message, posted_date FROM posts ORDER BY posted_date ASC";
            $total_result = @mysqli_query($dbcon, $total_query); // Run the query.

            //create new object pass in number of pages and identifier
            $pages = new Paginator(Config::PER_PAGE, 'page');
            $total = $total_result->num_rows;

            //pass number of records as total
            $pages->set_total($total);
            $query = "SELECT name, message, posted_date FROM posts ORDER BY posted_date ASC " . $pages->get_limit();
            $result = @mysqli_query($dbcon, $query); // Run the query.

            if ($result) { // If it runs
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $date = new DateTime();
                    $date->setTimestamp($row['posted_date']);
                    ?>
                    <div class="item">
                        <p class="desc"><?php echo $row['message'] ?></p>
                        <div class="bottom-area clearfix">
                            <div class="author">
                                <p class="name"><?php echo $row['name'] ?></p>
                                <span class="date"><?php echo $date->format('jS M, Y \a\t g:ia') ?></span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                mysqli_free_result($result); // Free up the resources.
            } else { // If it did not run OK.
                echo '<p class="error">The current users could not be retrieved. We apologize for any inconvenience.</p>';
            } // End of if ($r) IF.
            mysqli_close($dbcon); // Close the database connection.
            ?>
        </div>
        <div class="pagination-wrap">
            <?php echo $pages->page_links(); ?>
        </div>
        <?php include 'forms/frm-add-post.php'; ?>
        <?php include 'forms/frm-message.php'; ?>
    </div>
</div>
<script type='text/javascript' src="js/jquery.min.js"></script>
<script type='text/javascript' src="js/bootstrap.min.js"></script>
<script type='text/javascript' src="js/jquery.validate.min.js"></script>
<script type='text/javascript' src="js/home.js"></script>
</body>
</html>
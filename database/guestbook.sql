/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.31-MariaDB : Database - guestbook
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`guestbook` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `guestbook`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id`,`username`,`password`,`level`) values (1,'admin','21232f297a57a5a743894a0e4a801fc3',1);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `posted_date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `posts` */

insert  into `posts`(`id`,`name`,`message`,`posted_date`) values (40,'Vulputate eu','Vulputate eu scelerisque felis imperdiet proin fermentum. Tellus elementum sagittis vitae et leo duis ut diam quam. Praesent tristique magna sit amet. Dui accumsan sit amet nulla facilisi morbi tempus iaculis. Ultrices in iaculis nunc sed augue lacus. Eu non diam phasellus vestibulum lorem. Vulputate mi sit amet mauris commodo quis imperdiet massa tincidunt. Purus semper eget duis at tellus at.',1532012183),(30,'Tempor orci','Tempor orci dapibus ultrices in iaculis nunc sed augue. Tempor orci dapibus ultrices in iaculis nunc. Tincidunt vitae semper quis lectus. Pharetra et ultrices neque ornare aenean euismod elementum. Posuere ac ut consequat semper viverra nam libero.',1532011996),(31,'Ipsum dolor','Ipsum dolor sit amet consectetur adipiscing. Turpis cursus in hac habitasse platea dictumst quisque sagittis purus. Bibendum arcu vitae elementum curabitur vitae nunc. Laoreet non curabitur gravida arcu. Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Faucibus a pellentesque sit amet porttitor eget dolor morbi non. Purus sit amet volutpat consequat mauris.',1532012015),(32,'Tortor at auctor','Tortor at auctor urna nunc. Et ligula ullamcorper malesuada proin libero nunc consequat. Cursus metus aliquam eleifend mi in nulla posuere sollicitudin aliquam. Scelerisque fermentum dui faucibus in ornare. Viverra accumsan in nisl nisi scelerisque eu ultrices. Egestas purus viverra accumsan in. A erat nam at lectus urna duis convallis. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi.',1532012034),(33,'Nulla at volutpat','Nulla at volutpat diam ut venenatis tellus in metus vulputate. Penatibus et magnis dis parturient montes nascetur ridiculus. A arcu cursus vitae congue mauris rhoncus. Justo laoreet sit amet cursus sit amet dictum. Non blandit massa enim nec dui nunc mattis enim ut. In hac habitasse platea dictumst. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare suspendisse.',1532012054),(34,'Quis hendrerit','Quis hendrerit dolor magna eget est lorem ipsum. In hendrerit gravida rutrum quisque non tellus orci. Sed augue lacus viverra vitae congue eu. Suscipit adipiscing bibendum est ultricies integer. Morbi tincidunt augue interdum velit euismod in. Quis ipsum suspendisse ultrices gravida. In metus vulputate eu scelerisque. Sit amet risus nullam eget. Euismod quis viverra nibh cras pulvinar mattis nunc.',1532012075),(35,'Ut porttitor leo','Ut porttitor leo a diam sollicitudin tempor id eu. Lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. Tempor orci dapibus ultrices in iaculis. Pretium aenean pharetra magna ac placerat vestibulum lectus. Ultricies tristique nulla aliquet enim. Cras semper auctor neque vitae. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare.',1532012091),(36,'Fermentum posuere','Fermentum posuere urna nec tincidunt. Odio facilisis mauris sit amet massa vitae. In eu mi bibendum neque egestas. Scelerisque varius morbi enim nunc faucibus a pellentesque. Elit pellentesque habitant morbi tristique senectus. Facilisis mauris sit amet massa vitae tortor condimentum lacinia.',1532012112),(37,'Viverra suspendisse','Viverra suspendisse potenti nullam ac tortor vitae purus faucibus. Faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. Magna etiam tempor orci eu lobortis elementum. Enim lobortis scelerisque fermentum dui faucibus in. Tortor pretium viverra suspendisse potenti nullam ac tortor vitae.',1532012131),(38,'Massa vitae','Massa vitae tortor condimentum lacinia quis vel eros donec. Ac tincidunt vitae semper quis lectus nulla at volutpat. Amet facilisis magna etiam tempor orci eu lobortis. Nisi est sit amet facilisis magna etiam tempor. Mattis pellentesque id nibh tortor id aliquet lectus proin. Etiam sit amet nisl purus. Lacus vel facilisis volutpat est.',1532012150),(39,'Nullam non nisi','Nullam non nisi est sit amet facilisis magna. Non odio euismod lacinia at quis risus sed vulputate. Sagittis nisl rhoncus mattis rhoncus urna neque. Tincidunt vitae semper quis lectus nulla at volutpat diam ut. Nulla pharetra diam sit amet nisl suscipit. Volutpat maecenas volutpat blandit aliquam. Iaculis eu non diam phasellus vestibulum lorem sed risus.',1532012168),(29,'Lorem ipsum','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed viverra tellus in hac habitasse platea dictumst vestibulum rhoncus. Vitae aliquet nec ullamcorper sit. Lacus vestibulum sed arcu non. Praesent semper feugiat nibh sed pulvinar proin gravida hendrerit.',1532011947),(28,'Lorem ipsum','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',1532011902);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

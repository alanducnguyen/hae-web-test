# hae-web-test
# The website has implemented based on the following requirements:
# DESCRIPTION
# - Create a guestbook to let visitors of a website submit messages. The messages must be visible on the site to everyone.
# - The site’s admin user should be able to delete and edit the messages.
# - The message should be submitted using AJAX rather than standard form submissiossssn.
# - The messages should be stored in an SQL database.
# - The HTML output should be valid HTML5 and responsive for different screen sizes.
# REQUIREMENTS
# - Language: PHP
# - Web server: Apache
# - SQL database server: Mysql
# - Operating system: Cross platform 
# - Third Party PHP Frameworks: i has implemented by native PHP
# - Third Party JavaScript and CSS libraries: Allowed, e.g. JQuery, Bootstrap
#

Setup guestbook website steps:
1. clone source code from my bitbucket repository by run this command
	1.1 assume that you clone source code in to this folder: D:\mxampp\sources
	1.2 go to D:\mxampp\sources, open command line or git bash to run this command:
	
		git clone https://alanducnguyen@bitbucket.org/alanducnguyen/hae-web-test.git guestbook
	
2. add your host at C:\Windows\System32\drivers\etc\hosts
	127.0.0.1 guestbook.local
	
3. configure your vhost
	########### guestbook.local #############
	<VirtualHost *:80>
		ServerAdmin youremail@gmail.com
		DocumentRoot D:\mxampp\sources\guestbook
		ServerName  guestbook.local
		ErrorLog D:/mxampp/logs/guestbook.local-error.log
		CustomLog D:/mxampp/logs/guestbook.local_log common
		<Directory "D:\mxampp\sources\guestbook">
			Options Indexes FollowSymLinks MultiViews
			AllowOverride All
			Order allow,deny
			allow from all
			Require all granted
		</Directory>
	</VirtualHost>
	########### guestbook.local #############
	
4. import mysql database:
	4.1 database script is located at "D:\mxampp\sources\guestbook\guestbook.sql"
	4.2 run content in guestbook.sql to create database and it's sample data.
	
5. restart your apache server to reload virtual host configuration.

6. go to browser run "http://guestbook.local/" to enjoy the site.

7. The admin account has been setup for this site is: admin/admin
	note: password field using md5 for checking and hashing password.
	you can update any password you want to by using md5 hashing.
	
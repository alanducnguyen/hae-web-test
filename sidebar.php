<div class="sidebar">
    <h1 class="logo"><a href="index.php"><img src="images/logo.png" alt="HAE"></a></h1>
    <span class="guestbook">Guestbook</span>
    <p class="desc">Feel free to leave us a short message to tell us what you think to our services</p>
    <button type="button" class="btn btn-primary btn-add-post">Post a Message</button>
    <?php
    if (isset($_SESSION['username'])) { ?>
        <a href="admin.php" class="home-link">Go to Admin</a>
        <a href="logout.php" class="login-link">Admin Logout</a>
    <?php } else { ?>
        <a href="login.php" class="login-link">Admin Login</a>
    <?php } ?>
</div>
<?php

// This section processes submissions from the login form.
// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Check a POST is valid.
    if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
        //connect to database
        require('mysqli_connect.php');
        $errors = array();

        // Validate the username:
        if (!empty($_POST['username'])) {
            $username = mysqli_real_escape_string($dbcon, $_POST['username']);
        } else {
            $username = FALSE;
            $errors['username'] = 'You forgot to enter your username.';
        }
        // Validate the password:
        if (!empty($_POST['password'])) {
            $password = mysqli_real_escape_string($dbcon, $_POST['password']);
        } else {
            $password = FALSE;
            $errors['password'] = 'You forgot to enter your password.';
        }
        if ($username && $password) {//if no problems
            // Retrieve the id, username and level for that email/password combination:
            $query = "SELECT id, username, level FROM admin WHERE (username='$username' AND password=md5('$password'))";
            //run the query and assign it to the variable $result
            $result = mysqli_query($dbcon, $query);
            // Count the number of rows that match the email/password combination
            if (@mysqli_num_rows($result) == 1) {//The user input matched the database record
                // Start the session, fetch the record and insert the three values in an array
                session_start();
                $user_info = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $_SESSION['username'] = $user_info['username'];
                $url = 'admin.php';
                header('Location: ' . $url); // Make  the browser load either the members or the admin page
                mysqli_free_result($result);
                mysqli_close($dbcon);
                exit();
            } else { // No match was made.
                $errors['message'] = 'The username and password entered do not match our records.';
            }
        } else { // If there was a problem.
            $errors['message'] = 'The username and password entered is invalid.';
        }
        mysqli_close($dbcon);
    }else{
        $errors['message'] = 'Your post request is invalid.';
    }

} // End of SUBMIT conditional.
?>
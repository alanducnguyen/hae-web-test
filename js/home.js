jQuery(document).ready(function () {
    $.ajaxSetup({
        data: window.csrf
    });
    $(document).on('click', '.btn-add-post', function () {
        /* open add form*/
        $('#add-post-modal').modal();

        /* simple validate for the add form*/
        $("#frm-add-post").validate({
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                name: "required",
                message: "required",
            },
            // Specify validation error messages
            messages: {
                name: "Please enter your user name",
                message: "Please enter your message",
            },
            submitHandler: function (form) {
                /*binding submit action for frm-add-post*/
                var data = $(form).serialize();
                $.ajax({
                    url: $(form).attr('action'),
                    data: data,
                    dataType: 'json',
                    type: 'POST',
                    success: function (response) {
                        $('#add-post-modal').modal('hide');

                        if (response.success) {
                            $(".error-message").removeClass('error').addClass('success')
                        } else {
                            $(".error-message").removeClass('success').addClass('error');
                        }

                        $(".error-message").html(response.message);
                        $('#message-modal').modal();
                    },
                    error: function (xhr, status, error) {
                        $('html, body').animate({scrollTop: 0}, 'slow');
                    }
                });
                //e.preventDefault();
                return false;
            }
        });

        $("#message-modal").on("hidden.bs.modal", function () {
            window.location.reload(true);
        });
    });
});
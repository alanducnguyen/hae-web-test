jQuery(document).ready(function () {
    $(document).on('click', '.fa-pencil', function () {
        var item = $(this).parents('.item');
        /* remove the current item status from items then binding current status to the selected item*/
        $('.item').removeClass('current-item');
        $(item).addClass('current-item');

        /*retrieve current data in clicked item*/
        var id = $(item).data('post-id');
        var desc = $(item).find('.desc');
        var name = $(item).find('.name');

        /* binding the selected data into edit form*/
        $('#frm-edit-post').find('#name').val($(name).html());
        $('#frm-edit-post').find('#message').val($(desc).html());
        $('#frm-edit-post').find('#id').val(id);

        /* simple validate for the edit form*/
        $("#frm-edit-post").validate({
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                name: "required",
                message: "required",
            },
            // Specify validation error messages
            messages: {
                username: "Please enter your user name",
                message: "Please enter your message",
            }
        });

        /* open edit form*/
        $('#edit-post-modal').modal();

        /*binding submit action for frm-edit*/
        $('#frm-edit-post').on('submit', function (e) {
            var data = $('#frm-edit-post').serialize();
            $.ajax({
                url: $('#frm-edit-post').attr('action'),
                data: data,
                dataType: 'json',
                type: 'POST',
                success: function (response) {
                    $('#edit-post-modal').modal('hide');
                    if (response.success) {
                        $(".error-message").removeClass('error').addClass('success')
                    } else {
                        $(".error-message").removeClass('success').addClass('error');
                    }
                    /* update the editable data immediately*/
                    $('.current-item').find('.desc').html($('#frm-edit-post').find('#message').val())
                    $('.current-item').find('.name').html($('#frm-edit-post').find('#name').val());

                    $(".error-message").html(response.message);
                    $('#message-modal').modal();
                },
                error: function (xhr, status, error) {
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            });
            e.preventDefault();
        });
    });

    $("#message-modal").on("hidden.bs.modal", function () {
        window.location.reload(true);
    });

    $(document).on('click', '.fa-trash', function () {
        var item = $(this).parents('.item');
        var id = $(item).data('post-id');

        simpleDialogBox("Are you sure delete this record?", function () {
            var data = {id: id, sure: 'Yes'};
            $.ajax({
                url: 'delete.php',
                data: data,
                dataType: 'json',
                type: 'POST',
                success: function (response) {
                    $('#edit-post-modal').modal('hide');
                    if (response.success) {
                        $(".error-message").removeClass('error').addClass('success')
                    } else {
                        $(".error-message").removeClass('success').addClass('error');
                    }
                    /* update the editable data immediately*/
                    $('.current-item').find('.desc').html($('#frm-edit-post').find('#message').val())
                    $('.current-item').find('.name').html($('#frm-edit-post').find('#name').val());

                    $(".error-message").html(response.message);
                    $('#message-modal').modal();
                },
                error: function (xhr, status, error) {
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            });
            return false;
        }, function () {
        });
    });

});
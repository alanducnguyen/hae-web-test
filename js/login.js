jQuery(document).ready(function () {
    $("#frm-login").validate({
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            username: "required",
            password: {
                required: true,
                minlength: 4
            }
        },
        // Specify validation error messages
        messages: {
            username: "Please enter your user name",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 4 characters long"
            }
        }
    });
});
<?php
// This creates a connection to the logindb database and to MySQL,
include("config.php");
// Make the connection:
$dbcon = @mysqli_connect(Config::DB_HOST, Config::DB_USER, Config::DB_PASSWORD, Config::DB_NAME) OR die ('Could not connect to MySQL: ' . mysqli_connect_error());

// Set the encoding...
mysqli_set_charset($dbcon, 'utf8');
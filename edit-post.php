<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

require_once('util.php');
//The link to the database is moved to the top of the PHP code.
require('mysqli_connect.php'); // Connect to the db.

if (Util::isAjax()) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        // Check a POST is valid.
        if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
            $errors = array();
            //  Check for the id
            if (empty($_POST['id'])) {
                $errors[] = 'You forgot to enter the id.';
            } else {
                $id = mysqli_real_escape_string($dbcon, trim($_POST['id']));
            }

            // Check for a name:
            if (empty($_POST['name'])) {
                $errors[] = 'You forgot to enter the name.';
            } else {
                $name = mysqli_real_escape_string($dbcon, trim($_POST['name']));
            }

            // Check for a message:
            if (empty($_POST['message'])) {
                $errors[] = 'You forgot to the message.';
            } else {
                $message = mysqli_real_escape_string($dbcon, trim($_POST['message']));
            }

            if (empty($errors)) { // If everything's OK.
                // Make the query:
                $q = "UPDATE posts SET name='$name', message='$message' WHERE id=$id LIMIT 1";
                $r = @mysqli_query($dbcon, $q);
                if (mysqli_affected_rows($dbcon) == 1) { // If it ran OK.
                    echo json_encode(array('success' => true, 'message' => 'The message has been updated.'));
                } else { // If it did not run OK.
                    echo json_encode(array('success' => true, 'message' => 'The message has not been updated.'));
                }
            } else { // Report the errors.
                echo json_encode(array('success' => false, 'message' => 'non-ajax action is not accepted by server.', 'errors' => $errors));
            } // End of if (empty($errors)) IF.
        }
    }
} else {
    echo json_encode(array('success' => false, 'message' => 'non-ajax action is not accepted by server.'));
}
exit();
?>
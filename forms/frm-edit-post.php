<!-- Edit Post Modal -->
<div class="modal fade" id="edit-post-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Message Form</h4>
            </div>
            <form action="edit-post.php" name="frm-edit-post" id="frm-edit-post" method="post">
                <div class="row">
                    <div class="col-25">
                        <label for="name">Name:</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="name" name="name" placeholder="The name">
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="message">Message:</label>
                    </div>
                    <div class="col-75">
                        <textarea id="message" name="message" class="post-message"></textarea>
                    </div>
                </div>
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
                <div class="row submit-row">
                    <input type="submit" value="Submit" class="btn">
                </div>
            </form>
        </div>

    </div>
</div>
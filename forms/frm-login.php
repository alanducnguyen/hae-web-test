<div id="login-popup" name="login-popup" class="popup popup-content">
    <h2>Login</h2>
    <div class="error-message">
        <?php
        if (isset($errors)) {
            foreach ($errors as $error) {
                ?>
                <p><?php echo $error; ?></p>
                <?php
            }
        }
        ?>
    </div>
    <form action="login.php" name="frm-login" id="frm-login" method="post">
        <div class="row">
            <div class="col-25">
                <label for="username">User Name:</label>
            </div>
            <div class="col-75">
                <input type="text" id="username" name="username" placeholder="Your user name"
                       value="<?php echo isset($_POST['username']) ? $_POST['username'] : ''; ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="password">Password:</label>
            </div>
            <div class="col-75">
                <input type="password" id="password" name="password"
                       placeholder="Your password" <?php echo isset($_POST['password']) ? $_POST['password'] : ''; ?>>
            </div>
        </div>
        <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
        <div class="row submit-row">
            <input type="submit" value="Submit" class="btn">
        </div>
    </form>
</div>
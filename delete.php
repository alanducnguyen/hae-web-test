<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

require_once('util.php');
//The link to the database is moved to the top of the PHP code.
require('mysqli_connect.php'); // Connect to the db.

if (Util::isAjax()) {
    // Check if the form has been submitted:
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if ($_POST['sure'] == 'Yes') { // Delete the record.
            $id = mysqli_real_escape_string($dbcon, trim($_POST['id']));
            // Make the query:
            $query = "DELETE FROM posts WHERE id=$id LIMIT 1";
            $r = @mysqli_query($dbcon, $query);
            if (mysqli_affected_rows($dbcon) == 1) { // If it ran OK.
                echo json_encode(array('success' => true, 'message' => 'The post has been deleted.'));
            } else { // If the query did not run OK.
                echo json_encode(array('success' => false, 'message' => 'The post could not be deleted due to a system error.'));
            }
        } else { // No confirmation of deletion.
            echo json_encode(array('success' => false, 'message' => 'The post has NOT been deleted.'));
        }
    }
} else {
    echo json_encode(array('success' => false, 'message' => 'non-ajax action is not accepted by server.'));
}
exit();
?>
<?php
session_start();
require_once('util.php');
//The link to the database is moved to the top of the PHP code.
require('mysqli_connect.php'); // Connect to the db.

if (Util::isAjax()) {
    // This query INSERTs a record in the users table.
    // Has the  the form been submitted?
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        // Check a POST is valid.
        if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
            $errors = array(); // Initialize an error array.
            // Check for a name:
            if (empty($_POST['name'])) {
                $errors[] = 'You forgot to enter your name.';
            } else {
                $name = mysqli_real_escape_string($dbcon, trim($_POST['name']));
            }
            // Check for a last name:
            if (empty($_POST['message'])) {
                $errors[] = 'You forgot to enter your message.';
            } else {
                $message = mysqli_real_escape_string($dbcon, trim($_POST['message']));
            }

            if (empty($errors)) { // If it runs
                // Make the query:
                $posted_date = strtotime('now');
                $query = "INSERT INTO posts (name, message, posted_date) 
                  VALUES ('$name', '$message', $posted_date )";

                $result = @mysqli_query($dbcon, $query); // Run the query.
                if ($result) {
                    echo json_encode(array('success' => true, 'message' => 'The message has been added.', 'errors' => $errors));
                } else { // If it did not run
                    echo json_encode(array('success' => false, 'message' => 'there is error happen in database server, please contact admin to check server log.'));
                } // End of if ($result)
                mysqli_close($dbcon); // Close the database connection.
                exit();
            } else { // Report the errors.
                echo json_encode(array('success' => false, 'message' => 'The message has not been added.', 'errors' => $errors));
            }// End of if (empty($errors))
        }
    } // End of the main Submit conditional.

} else {
    echo json_encode(array('success' => false, 'message' => 'non-ajax action is not accepted by server.'));
}
exit();
?>
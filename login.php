<?php
session_start();

// Create a new CSRF token.
if (! isset($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
}

if (isset($_SESSION['username']))
{
    header("Location: admin.php");
    exit();
}
?>
<!doctype html>
<html lang=en>
<head>
    <title>Login</title>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <script type="text/javascript">
        window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    </script>
</head>
<body>
<?php include 'include/login-inc.php'; ?>
<div class="wrapper">
    <?php include("forms/frm-login.php"); ?>
</div>
<script type='text/javascript' src="js/jquery.min.js"></script>
<script type='text/javascript' src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src='js/login.js'></script>
</body>
</html>